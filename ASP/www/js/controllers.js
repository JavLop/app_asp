angular.module('starter.controllers', [])

.controller('AppCtrl', function($scope, $ionicModal, $timeout) {

    // With the new view caching in Ionic, Controllers are only called
    // when they are recreated or on app start, instead of every page change.
    // To listen for when this page is active (for example, to refresh data),
    // listen for the $ionicView.enter event:
    //$scope.$on('$ionicView.enter', function(e) {
    //});

    // Form data for the login modal
    $scope.loginData = {};

    // Create the login modal that we will use later
    $ionicModal.fromTemplateUrl('templates/login.html', {
        scope: $scope
    }).then(function(modal) {
        $scope.modal = modal;
    });

    // Triggered in the login modal to close it
    $scope.closeLogin = function() {
        $scope.modal.hide();
    };

    // Open the login modal
    $scope.login = function() {
        $scope.modal.show();
    };

    // Perform the login action when the user submits the login form
    $scope.doLogin = function() {
        console.log('Doing login', $scope.loginData);

        // Simulate a login delay. Remove this and replace with your login
        // code if using a login system
        $timeout(function() {
            $scope.closeLogin();
        }, 1000);
    };
})

.controller('PlaylistsCtrl', function($scope) {
    $scope.playlists = [
      { title: 'Consulta de Saldo', id: 1 },
      { title: 'Consulta de Gestiones', id: 2 },
      { title: 'Dubstep', id: 3 },
      { title: 'Indie', id: 4 },
      { title: 'Rap', id: 5 },
      { title: 'Cowbell', id: 6 }
    ];
})

.controller('gestionCtrl', function($scope, $http) {
    $http.get("http://localhost:16350/api/website/Ordenes?Data=1000", { cache: true })
    .then(function(response){
        $scope.servicio = {};
        $scope.gestion = {};
        $scope.servicio = response.data;

        console.log($scope.servicio.Data.data[0].NIDREGORD);
        console.log($scope.servicio.Data.data[0].NIDREGORD_CDS);

        //var json = JSON.parse(response.data);
        //$scope.gestion = JSON.parse(response.data[0]);

        //console.log(json);
    });

    $scope.consultaGestion = function() {
        
        var idGestion = document.getElementById('NumeroGestion_Id').value
        var url = "http://localhost:16350/api/website/Ordenes?Data=" + idGestion;

        $http.get(url, { cache: true })
        .then(function(response){
            $scope.servicio = {};
            $scope.servicio = response.data;

            $scope.idOrden= $scope.servicio.Data.data[0].NIDREGORD;
            $scope.idDescripcion = "";
            $scope.Fecha = $scope.servicio.Data.data[0].FECHA;
            $scope.FechaDescripcion = "";
            $scope.FechaCierre = $scope.servicio.Data.data[0].TDATE_CL;
            $scope.FechaCierreDescripcion = "";
            $scope.Estado = $scope.servicio.Data.data[0].ESTADO;
            $scope.EstadoDescripcion = "";
            $scope.Gestion = $scope.servicio.Data.data[0].GESTION;
            $scope.GestionDescripcion = "";

            console.log($scope.servicio.Data.data[0].NIDREGORD);
            console.log("EN la funcion");

        });
    };

})


.controller('PlaylistCtrl', function($scope, $stateParams) {
});
